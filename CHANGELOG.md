# Changelog

<!--next-version-placeholder-->

## v1.10.1 (2024-05-03)

### Fix

* Jobscripts now request scratch/gdata access ([`35099cd`](https://gitlab.com/chilton-group/hpc_suite/-/commit/35099cd032eb9bfce3989eb4b0424632b508281b))

## v1.10.0 (2024-05-03)

### Feature

* Gadi support ([`391ce0c`](https://gitlab.com/chilton-group/hpc_suite/-/commit/391ce0c1ff89c2e3dabd4e4764179f3b33826eda))
* Gadi support ([`e3a81c0`](https://gitlab.com/chilton-group/hpc_suite/-/commit/e3a81c043e8f86f837036a3fe6631ba8963a7be4))
* PBS/Gadi support ([`ba255ed`](https://gitlab.com/chilton-group/hpc_suite/-/commit/ba255edd694b10b252c74c0ef63c58bb0d59da76))

## v1.9.0 (2023-08-09)

### Feature

* Integer index parser ([`eaf4b9a`](https://gitlab.com/chilton-group/hpc_suite/-/commit/eaf4b9a6d2ce9f7df96fcef0b871a386a43d38cc))

### Fix

* Module purge ([`9a18ca7`](https://gitlab.com/chilton-group/hpc_suite/-/commit/9a18ca74df17df6a338aeaee77efc2b91e3dc385))

## v1.8.1 (2023-03-30)


## v1.8.0 (2023-02-20)
### Feature
* Allow for non-file sources ([`de1d3b0`](https://gitlab.com/chilton-group/hpc_suite/-/commit/de1d3b03d65646752a97b9d127e8e5bb6328778c))

## v1.7.1 (2022-09-28)
### Fix
* Minor bug in dict parser type ([`0f4641e`](https://gitlab.com/chilton-group/hpc_suite/-/commit/0f4641e75fa82780660a00605a436089b77ab187))

## v1.7.0 (2022-09-15)
### Feature
* Add short queue node type ([`1d25034`](https://gitlab.com/chilton-group/hpc_suite/-/commit/1d25034095b8d8524eea857b7c6a4555e70f4b71))

## v1.6.0 (2022-08-17)
### Feature
* Make_parse_dict generator ([`49f1dd7`](https://gitlab.com/chilton-group/hpc_suite/-/commit/49f1dd7a06a7db08dffd1521f0215484a380d44a))

## v1.5.0 (2022-08-12)
### Feature
* Implement row names functionality ([`7e1c068`](https://gitlab.com/chilton-group/hpc_suite/-/commit/7e1c06848d0dc4aefe5401ed250ba1ca209e3be1))

### Fix
* Hdf5 writing with names bug ([`d41897a`](https://gitlab.com/chilton-group/hpc_suite/-/commit/d41897abc282409104da0425d2698dbe8f5ad362))
* Remove size limitation for row_names attribute ([`737d3a6`](https://gitlab.com/chilton-group/hpc_suite/-/commit/737d3a6f191bb0c847c38349639a78e90fd11eb5))

## v1.4.0 (2022-08-01)
### Feature
* Add ParseKwargsAppend action ([`95e2b2d`](https://gitlab.com/chilton-group/hpc_suite/-/commit/95e2b2d8b1440c7dffbc86673568f0918068c9e8))

## v1.3.3 (2022-07-20)
### Fix
* Export OMP_NUM_THREADS var for csf3 and csf4 ([`39c11b9`](https://gitlab.com/chilton-group/hpc_suite/-/commit/39c11b9de0be9ce4404851574c3606acce88463c))

## v1.3.2 (2022-06-08)


## v1.3.1 (2022-06-08)


## v1.3.0 (2022-04-29)
### Feature
* Support for automatically getting hostname ([`f0c95d6`](https://gitlab.com/chilton-group/hpc_suite/-/commit/f0c95d64d199041cfafc117d54abb5ff0bcc8a94))
* Modify profile to include csf4 choice ([`73e05af`](https://gitlab.com/chilton-group/hpc_suite/-/commit/73e05af545b61927928d67b68f19af2ac47cdef9))

### Fix
* Remove debug printing ([`2833ce8`](https://gitlab.com/chilton-group/hpc_suite/-/commit/2833ce8c4cb5d695f64ad8d07ca7949dc574da4b))
* Correct class instantiation in dictionary ([`50beb5b`](https://gitlab.com/chilton-group/hpc_suite/-/commit/50beb5b4b4f86cd34737bf7018e27b36be42b905))
* Fix support for omp jobs on cerberus and medusa ([`b7747b9`](https://gitlab.com/chilton-group/hpc_suite/-/commit/b7747b9c11845c14d5c02ff8af095cd58af407cd))
* Remove git merge conflict characters ([`884f8df`](https://gitlab.com/chilton-group/hpc_suite/-/commit/884f8df2002ce6aff32d8d122e053deae33536a9))

## v1.2.0 (2022-03-21)
### Feature
* Store message printing and quiet ([`a772ae9`](https://gitlab.com/chilton-group/hpc_suite/-/commit/a772ae9051229a9da77d974fe31be5a13fc6a73f))

## v1.1.0 (2022-02-08)
### Feature
* Add key-only class ([`9fdce58`](https://gitlab.com/chilton-group/hpc_suite/-/commit/9fdce584bccfd09493efddfa253f99e9cd87f32a))

## v1.0.2 (2022-02-08)


## v1.0.1 (2021-11-26)
### Fix
* Bugfixes and documentation of Store classes ([`504290b`](https://gitlab.com/chilton-group/hpc_suite/-/commit/504290bb5075b998868a43bf3a1e8b8d95feb824))

## v1.0.0 (2021-11-18)
### Feature
* Add keep_all filter ([`8b1c964`](https://gitlab.com/chilton-group/hpc_suite/-/commit/8b1c9646ec8137e83aa4a7fd6f4e087e047beeec))
* Initial implementation of store functionality ([`e7df9b3`](https://gitlab.com/chilton-group/hpc_suite/-/commit/e7df9b3c6d0fdde647f504cc7d8010292a91e2b0))

### Breaking
* increment major version to 1.0.0 ([`6ec5c3c`](https://gitlab.com/chilton-group/hpc_suite/-/commit/6ec5c3ca408509d945eef8163e050c3da9543b6c))
* version 1.0.0 ([`4a1088e`](https://gitlab.com/chilton-group/hpc_suite/-/commit/4a1088e44f3c1a030b04c6fafd43fdde4bc43010))

### Documentation
* Documentation of cli interface to store functionality ([`807a4a7`](https://gitlab.com/chilton-group/hpc_suite/-/commit/807a4a7925d9b6c6a6d40a3d38f324432cd2e562))
* Documentation of storage interface ([`4bfcc3c`](https://gitlab.com/chilton-group/hpc_suite/-/commit/4bfcc3c05fce76fa2b7afb13c0563094dd45c851))

## v0.0.1 (2021-11-03)
### Fix
* Rename main to master ([`ebe1328`](https://gitlab.com/chilton-group/hpc_suite/-/commit/ebe1328c76a6d682c4d4248580d9596639c461bd))
* Main back to master ([`68aa8a0`](https://gitlab.com/chilton-group/hpc_suite/-/commit/68aa8a0396a6ed1b2b9bed24db42fdf46160f49f))
* Change master to main ([`e3af1ad`](https://gitlab.com/chilton-group/hpc_suite/-/commit/e3af1adfd40584f8ebd4427f007f63b6560208e2))
* Incorrect triggering criteria ([`055f04c`](https://gitlab.com/chilton-group/hpc_suite/-/commit/055f04cd0bf66eb51403f0525f2683dc869fa866))
